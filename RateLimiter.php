<?php

class RateLimiter
{
    const MAX_ATTEMPTS = 5;
    const PER_MINUTES = 1; //minutes
    const BLOCKED_TIME = 10; //minutes

    protected $redis;

    function __construct(Redis $redis)
    {
        $this->redis = $redis;
    }

    /**
     * @return bool
     */
    public function tooManyAttempts()
    {
        $key = $this->getKey();
        if ($this->areTooManyAttempts($key, self::MAX_ATTEMPTS)) {
            http_response_code(429);
            header(sprintf("Retry-After: %d", floor($this->redis->ttl($key . ':timer'))));
            return true;
        }
        $this->hit($key, self::MAX_ATTEMPTS);
        return false;
    }

    /**
     * @param $key
     * @param $maxAttempts
     * @return bool
     */
    public function areTooManyAttempts($key, $maxAttempts)
    {
        if ($this->attempts($key) >= $maxAttempts) {
            if ($this->redis->get($key . ':timer')) {
                return true;
            }
            $this->clear($key);
        }
        return false;
    }

    /**
     * @param $key
     * @param $maxAttempts
     */
    public function hit($key, $maxAttempts)
    {
        if (!$this->redis->exists('lastAttempt')) {
            $this->redis->set('lastAttempt', time());
        }

        if (time() - $this->redis->get('lastAttempt') <= self::PER_MINUTES * 60) {
            $this->redis->incr($key);
        } else {
            $this->redis->decr($key);
        }
        $this->redis->set('lastAttempt', time());

        if ($this->attempts($key) == $maxAttempts) {
            $this->redis->set($key . ':timer', time());
            $this->redis->expire($key . ':timer', self::BLOCKED_TIME * 60);
        }
    }

    /**
     * @param $key
     */
    public function clear($key)
    {
        $this->redis->del($key . ':timer');
        $this->redis->del($key);
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return sha1($_SERVER['REMOTE_ADDR']);
    }

    /**
     * @param $key
     * @return bool|mixed|string
     */
    public function attempts($key)
    {
        if (!$this->redis->exists($key)) {
            $this->redis->set($key, 0);
        }
        return $this->redis->get($key);
    }


}