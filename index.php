<?php
require_once('RateLimiter.php');
try {
    $redis = new Redis();
    $redis->connect("127.0.0.1", 6379);
    $rateLimiter = new RateLimiter($redis);
    if (!$rateLimiter->tooManyAttempts()) {
        print_r('Hello World');
    }
} catch (Exception $e) {
    die($e->getMessage());
}
